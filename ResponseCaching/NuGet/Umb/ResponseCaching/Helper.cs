﻿using System;
using umbraco.DataLayer;
using umbraco.BusinessLogic;
using log4net;
using System.Web;
using System.Linq;
using System.IO;
using System.Text.RegularExpressions;
using Umbraco.Core;
using System.Web.Hosting;


namespace TamTam.NuGet.Umb.ResponseCaching
{
    public static class Helper
    {
        private static string _connectionString = umbraco.GlobalSettings.DbDSN;

        public static DateTime GetLastModifiedDate()
        {
            var lastModified = DateTime.Now;
            try
            {
                //First and quickest way is to check the umbraco.config file timestamp
                var file = HostingEnvironment.MapPath("/App_Data/umbraco.config");
                if (File.Exists(file))
                {
                    lastModified = File.GetLastWriteTime(file);
                }
                else
                {
                    //Get a connection and check if there is any record since that date that was published, moved or deleted.
                    //This relies heavily on some other routines that make sure that media that is saved also result in log entries.
                    var db = ApplicationContext.Current.DatabaseContext.Database;
                    lastModified = db.ExecuteScalar<DateTime>("SELECT MAX(DateStamp) FROM umbracoLog WHERE (logHeader = 'Publish' OR logHeader = 'Delete' OR logHeader = 'Move')");
                }
            }
            catch (Exception ex)
            {
                //Log exception and return that something changed (better be save then sorry)
                ILog logger = LogManager.GetLogger(string.Empty);
                logger.Error(ex);
            }
            return new DateTime(lastModified.Year, lastModified.Month, lastModified.Day, lastModified.Hour, lastModified.Minute, lastModified.Second);
        }

        public static bool RequestIsElligableFor304(HttpRequest request, out string max304Age)
        {
            var config = Settings.GetCacheSettings(request);
            if (config != null)
            {
                max304Age = config.Max304Age;
                return (!config.Disable && config.Enable304);
            }
            max304Age = string.Empty;
            return false;
        }

        public static bool RequestIsElligableForMaxAge(HttpRequest request, out int maxAge)
        {
            var config = Settings.GetCacheSettings(request);
            if (config != null)
            {
                maxAge = config.MaxAge;
                return (!config.Disable && maxAge != 0);
            }
            maxAge = 0;
            return false;
        }

        public static void Handle304(Boolean throw304, DateTime lastModifiedDate, HttpContext context)
        {
            ILog logger = LogManager.GetLogger(string.Empty);

            //if nothing changed: 304 it is
            if (throw304)
            {
                //Set the header information and complete the request
                context.Response.StatusCode = 304;
                context.ApplicationInstance.CompleteRequest();
#if DEBUG
                context.Response.AddHeader("Returned-Status", "304");
#endif
                logger.Debug("ResponseCaching - Returned-Status: " + 304);
            }
            else
            {
                //Set the lastmodified date in the response (if a 304 was sent it will still work with the old modified-since date)
                //Besides: for the 304 persistance rule we want it to keep requesting with the orginal value
                context.Response.Cache.SetLastModified(lastModifiedDate);
#if DEBUG
                context.Response.AddHeader("Returned-Status", "200");
#endif
                logger.Debug("ResponseCaching - Returned-Status: " + 200);
            }
        }
    }
}