﻿using System;
using System.Web;
using System.Web.Hosting;
using umbraco.BusinessLogic;
using Umbraco.Core;
using Umbraco.Core.Auditing;
using Umbraco.Core.Models;
using Umbraco.Core.Services;
using u = Umbraco;

namespace TamTam.NuGet.Umb.ResponseCaching
{
    public class Startup : IApplicationEventHandler
    {
        public void OnApplicationInitialized(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            //Nothing
        }

        public void OnApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            MediaService.Saved += MediaService_Saved;
        }

        void MediaService_Saved(IMediaService sender, u.Core.Events.SaveEventArgs<IMedia> e)
        {
            var file = HostingEnvironment.MapPath("/App_Data/umbraco.config");
            if (System.IO.File.Exists(file))
            {
                System.IO.File.SetLastWriteTime(file, DateTime.Now);
            }

            var userId = -1;
            var user = User.GetCurrent();
            if (user != null) { userId = user.Id; };
            foreach (var item in e.SavedEntities)
            {
                Audit.Add(AuditTypes.Publish, "Media saved and published", userId, item.Id);
            }
        }

        public void OnApplicationStarting(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            //Nothing
        }
    }
}
