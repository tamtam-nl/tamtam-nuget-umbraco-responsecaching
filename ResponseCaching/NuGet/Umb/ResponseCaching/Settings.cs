﻿using System.IO;
using System.Web;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System;

namespace TamTam.NuGet.Umb.ResponseCaching
{
    public static class Settings
    {
        public static CacheSettings MainSettings = new CacheSettings();
        public static List<CacheSettings> Configs = new List<CacheSettings>();

        /// <summary>
        /// Constructor: Reads all the settings from file and save them to field variables
        /// </summary>
        static Settings()
        {
            //Get the configuration file and read the settings
            var file = Path.Combine(HttpRuntime.AppDomainAppPath, "config\\TamTam.Nuget.Umb.ResponseCaching.config");
            var settings = XDocument.Load(file).Element("settings").Element("responseCaching");

            //Read the standard values from the file
            MainSettings.Disable = bool.Parse(settings.GetValue("disable") ?? "false");
            MainSettings.Enable304 = bool.Parse(settings.GetValue("enable304") ?? "false");
            MainSettings.Max304Age = settings.GetValue("max304Age") ?? string.Empty;
            MainSettings.MaxAge = int.Parse(settings.GetValue("maxAge") ?? "0");
            MainSettings.Methods = (settings.GetValue("methods") ?? string.Empty).Split(',');
            MainSettings.UserAgentsRegex = settings.GetValue("userAgentsRegex") ?? string.Empty;
            MainSettings.VaryByHost = bool.Parse(settings.GetValue("varyByHost") ?? "false");
            MainSettings.Scope = (CacheSettings.CacheScope)Enum.Parse(typeof(CacheSettings.CacheScope), settings.GetValue("scope") ?? "public", true);

            //Read the paths and create a dictionary
            foreach (var config in settings.Element("configs").Elements("config"))
            {
                Configs.Add(new CacheSettings()
                {
                    PathRegex = config.GetValue("pathRegex"),
                    Disable = bool.Parse(config.GetValue("disable") ?? MainSettings.Disable.ToString()),
                    Enable304 = bool.Parse(config.GetValue("enable304") ?? MainSettings.Enable304.ToString()),
                    Max304Age = config.GetValue("max304Age") ?? MainSettings.Max304Age,
                    MaxAge = int.Parse(config.GetValue("maxAge") ?? MainSettings.MaxAge.ToString()),
                    Methods = (config.GetValue("methods") ?? string.Join(",", MainSettings.Methods)).Split(','),
                    UserAgentsRegex = config.GetValue("userAgentsRegex") ?? MainSettings.UserAgentsRegex,
                    VaryByHost = bool.Parse(config.GetValue("varyByHost") ?? MainSettings.VaryByHost.ToString()),
                    Scope = (CacheSettings.CacheScope)Enum.Parse(typeof(CacheSettings.CacheScope), config.GetValue("scope") ?? MainSettings.Scope.ToString(), true)
                });
            }
        }

        public static Settings.CacheSettings GetCacheSettings(HttpRequest request)
        {
            string path = request.Url.AbsolutePath;
            string method = (request.HttpMethod ?? string.Empty).ToUpper();
            string userAgent = (request.UserAgent ?? string.Empty).ToLower();
            var config = Settings.Configs.FirstOrDefault(c => Regex.IsMatch(path, c.PathRegex, RegexOptions.IgnoreCase) && (string.IsNullOrEmpty(c.UserAgentsRegex) || Regex.IsMatch(userAgent, c.UserAgentsRegex, RegexOptions.IgnoreCase)) && (c.Methods.Length == 0 || c.Methods.Any(m => m.ToUpper() == method)));
            if (config != null)
            {
                return config;
            }
            else if ((string.IsNullOrEmpty(MainSettings.UserAgentsRegex) || Regex.IsMatch(userAgent, MainSettings.UserAgentsRegex, RegexOptions.IgnoreCase)) && (MainSettings.Methods.Length == 0 || MainSettings.Methods.Any(m => m.ToUpper() == method)))
            {
                return MainSettings;
            }
            return null;
        }


        /// <summary>
        /// Extension methof that returns the value of an attribute in an element if the attribute exists and does not have an empty or null string
        /// </summary>
        /// <param name="e">the extended object</param>
        /// <param name="name">the name of the attribute</param>
        /// <returns></returns>
        private static string GetValue(this XElement e, string name)
        {
            var attribute = e.Attribute(name);
            if (attribute != null)
            {
                var value = attribute.Value;
                if (!string.IsNullOrEmpty(value))
                {
                    return value;
                }
            }
            return null;
        }

        public class CacheSettings
        {
            public enum CacheScope
            {
                Private = 1,
                Public = 2
            }
            public string PathRegex;
            public bool Disable;
            public bool Enable304;
            public string Max304Age;
            public int MaxAge;
            public string[] Methods;
            public string UserAgentsRegex;
            public bool VaryByHost;
            public CacheScope Scope;
        }


    }
}
