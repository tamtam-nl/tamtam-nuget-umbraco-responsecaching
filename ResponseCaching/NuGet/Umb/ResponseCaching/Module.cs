﻿using System;
using System.Web;
using System.Linq;
using log4net;
using System.Text.RegularExpressions;

namespace TamTam.NuGet.Umb.ResponseCaching
{
    public class Module : IHttpModule
    {
        private HttpApplication _application;

        /// <summary>
        /// Disposes any memory allocations that are not autmoatically cleaned up by the garbage collector
        /// </summary>
        public void Dispose()
        {
        }

        public void Init(HttpApplication application)
        {
            //register the application object and attach event handlers
            _application = application;
            _application.PreRequestHandlerExecute += new EventHandler(OnPreRequestHandlerExecute);
        }

        /// <summary>
        /// Checks the current page request for cache headers and sets 304, max-age accordingly.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnPreRequestHandlerExecute(object sender, EventArgs e)
        {
            try
            {
                //Get the context, request and the response
                var context = _application.Context;
                var request = context.Request;
                var response = context.Response;
                var now = DateTime.Now;

                ILog logger = LogManager.GetLogger(string.Empty);
                logger.Debug("ResponseCaching - Checking request: " + request.RawUrl);

                ////First check if we shall set the max-age in the header
                int maxAge = 0;
                if (Helper.RequestIsElligableForMaxAge(request, out maxAge))
                {
                    //Always omit the vary=* (something ASP4ALL once said)
                    response.Cache.SetOmitVaryStar(true);

                    //Set the basic cache headr info for all elligable requests 
                    var requestSettings = Settings.GetCacheSettings(request);
                    response.Cache.SetCacheability(requestSettings.Scope == Settings.CacheSettings.CacheScope.Private ? HttpCacheability.Private : HttpCacheability.Public);
                    response.Cache.SetMaxAge(TimeSpan.FromSeconds(maxAge));
                    response.Cache.SetExpires(now.AddSeconds(maxAge));
                    response.Cache.VaryByParams["*"] = true;
                    if (requestSettings.VaryByHost)
                    {
                        response.Cache.SetVaryByCustom("host");
                    }
                    //response.Cache.SetSlidingExpiration(true);
                    logger.Debug("ResponseCaching - Max-Age set to: " + maxAge);

                }

                //Now we check if we should consider the url for a 304 header. If not we get out
                string max304Age = null;
                bool elligable = Helper.RequestIsElligableFor304(request, out max304Age);
                if (elligable)
                {
                    //Check and see if there is modified since value in the request header
                    var ims = request.Headers["If-Modified-Since"];

                    //Set the new date to now (we do this now so we avoid the possibility that during the next process the CMS got updated.
                    DateTime lastModifiedDate = Helper.GetLastModifiedDate();

                    //If there is a modified since in the header: check it against the last moment something changed in the CMS
                    bool throw304 = false;
                    if (ims != null)
                    {

                        //Remove a possible lenght portion on the ims given by some broswers.
                        var pos = ims.IndexOf(";length");
                        if (pos != -1)
                        {
                            ims = ims.Substring(0, pos);
                        }

                        logger.Debug("ResponseCaching - Checking if modified since: " + ims);

                        //Parse the given date and time
                        DateTime ifModifiedSinceDate = DateTime.MinValue;
                        ifModifiedSinceDate = DateTime.Parse(ims.ToString());

                        //Check the 304 persistance. (If not empty)
                        var tooOld = false;
                        if (!string.IsNullOrEmpty(max304Age))
                        {
                            //If it is an integer, it's a sliding persistance
                            int sliding;
                            if (int.TryParse(max304Age, out sliding))
                            {
                                var noOlderThen = now.AddMinutes(-1 * sliding);
                                if (ifModifiedSinceDate < noOlderThen)
                                {
                                    tooOld = true;
                                }
                            }
                            //If it is a time (h:mm) it is an absolute persistance
                            DateTime absolute;
                            if (DateTime.TryParse(max304Age, out absolute))
                            {
                                var nowMinutes = (now - now.Date).TotalMinutes;
                                var absMinutes = (absolute - absolute.Date).TotalMinutes;
                                var imsMinutes = (ifModifiedSinceDate - ifModifiedSinceDate.Date).TotalMinutes;
                                if (nowMinutes >= absMinutes && (ifModifiedSinceDate.Date < now.Date || (ifModifiedSinceDate.Date == now.Date && imsMinutes < absMinutes)))
                                {
                                    tooOld = true;
                                }
                            }
                        }

                        //If not too old (according to the mag 304 age) we will check whether the content changed.
                        if (!tooOld)
                        {
                            throw304 = (lastModifiedDate <= ifModifiedSinceDate);
                        }
                    }

                    //Call the routine to handle the 304 stuff
                    Helper.Handle304(throw304, lastModifiedDate, context);


                }
                logger.Debug("ResponseCaching - Finished checking request");
            }
            catch (Exception ex)
            {
                ILog logger = LogManager.GetLogger(string.Empty);
                logger.Error(ex);
            }
        }
    }
}