# RESPONSE CACHING

This package improves the overall performance of umbraco websites by adding a MaxAge and 304-modification date information to the response header. By comparing the modified since in the request header with the last write time to the Umbraco.config file it can determine if the page needs to be refreshed. If content it read directly from the database it will determine it using the last entry in the umbracoLog file. All workings can be tweaked using a configuration file.

### Rules of thumb (in FireFox) ###

1. A fresh request (not requested before) will serve a page from the IIS cache (output caching) if it was already requested from another browser.
2. Any click on a link to a page that was previously requested by the browser will not be requested again for the duration of the seconds in the max-age header
3. Pressing F5 to refresh the page will override the max-age header and send a request with the date and time the page was last modified. The server will use this to determine if the page needs to be rendered again. If not, it will send a 304 status telling to browser to use it's cached version.
4. Pressing ctrl-F5 sends a pragma-nocache in the header telling the server to serve the page again.

### Package

The packages installs the following:
* A configuration file in the Config folder.
* A Helper class
* A module class that will analyse each request coming in
* A settings class that loads the configuration file
* A startup class that registers event on application start-up.
* It adds a line to the modules section in the web.config

### Configuration

The packages is immediately ready after installation. However the following can be configured:

#### General settings 
|Attribute|Description
|---|---
| Disable| turns of all caching. Can be overruled by sub configs
| Enable304| Enables 304 caching invalidating cached urls when content in Umrbaco changes. Can be overruled by sub configs
| Max304Age| Absolute Date and time or TimeSpan indicating when umbraco content should alway be invalidated. Can be overruled by sub configs
| MaxAge| Total seconds content should be cached by IIS (output caching), any proxy/CDN and the browser. Use 0 to turn it off. Can be overruled by sub configs
| Methods| Methods that the cache settings are valid for. Can be overruled by sub configs
| UserAgents| UserAgents that the cache settings are valid for
| Scope| Public or Private. Set scope to private when using Incapsula (in standard/static mode). Set to public otherwise to use IIS for output caching

#### Special configuration per url pattern
| Attribute(s)| Description
|---|---
| PathRegex| A regular expression to identify whether the config is used for the requested url
| UserAgents and Methods| Extra filters for the specific confgiuration
| Disable, Enable304, Max304Age and MagAge| Settings to overrule the General settings

#### Rules per template or route
To turn off caching or change max-age for specific templates or routes you can set the wanted value at the sepecific location
in the code using Response.Cache.Cacheability and Response.Cache.SetMaxAge methods.
Keep in mind that the lowest cache setting is always granted when setting it more then once. 
E.g. 1: If you set the MaxAge in a template to 3600 and the MaxAge in this config is set to 1800, the header will contain 1800 (the lowest)
E.g. 2: If you set Cacheability to NoCache it will send a no-cache header regardless of any other code setting the cache.

#### Multiple Hostnames
Use varyByHost you have a single website in IIS supporting several websites in Umbraco with different HTML output. To make sure that each homepage is cached separately a special HTTP header has to be set. You can do this using the varyByHost attribute which you can overrule on sub confgigurations so images shared by all websites can use shared caching.

To make this work you need to add the following code to you Global.asax file (if not available you will have to create one)
      
	<source lang="csharp">
	public override string GetVaryByCustomString(HttpContext context, string custom)
	{
	    if (custom == "host")
	    {
	        return "Host=" + context.Request.Url.Host;
	    }
	    return base.GetVaryByCustomString(context, custom);
	}
	</source>

*ALTERNATIVELY YOU CAN CREATE A SEPARATE WEBSITE IN IIS FOR EACH BINDING WHILE SHARING THE SAME APP POOL*
